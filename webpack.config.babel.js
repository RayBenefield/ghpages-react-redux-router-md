/* eslint-disable import/no-extraneous-dependencies */
import path from 'path';
import webpack from 'webpack';

const BUILD_DIR = path.resolve(__dirname, 'build/');
const APP_DIR = path.resolve(__dirname, 'src/');

const config = {
    entry: [
        'react-hot-loader/patch',
        'webpack-hot-middleware/client',
        `${APP_DIR}/index.js`,
    ],
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: APP_DIR,
                loaders: ['babel'],
            },
            {
                test: /\.html$/,
                loader: 'file?name=[name].[ext]',
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
            },
            {
                test: /\.md$/,
                loader: 'raw',
            },
            {
                test: /\.css$/,
                loaders: [
                    'style?sourceMap',
                    'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
                ],
                exclude: [
                    path.resolve('node_modules'),
                ],
            },
            {
                test: /\.css$/,
                loader: 'file?name=[name].[ext]',
                include: [
                    path.resolve('node_modules'),
                ],
            },
        ],
    },
};

export default config;

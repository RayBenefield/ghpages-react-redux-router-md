/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
import path from 'path';
import webpack from 'webpack';
import express from 'express';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';
import config from './webpack.config.babel';

const app = express();
const compiler = webpack(config);

app.use(devMiddleware(compiler, {
    publicPath: config.output.publicPath,
    historyApiFallback: true,
}));

app.use(hotMiddleware(compiler));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(3000, (err) => {
    if (err) {
        return console.error(err);
    }

    console.log('Listening at http://localhost:3000/');
    return null;
});

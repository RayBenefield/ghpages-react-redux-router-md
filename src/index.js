/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */
import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from './app';

injectTapEventPlugin();

const mountApp = document.getElementById('app');

render(<AppContainer component={App} />, mountApp);

if (module.hot) {
    module.hot.accept('./app', () => {
        render(
            <AppContainer component={require('./app').default} />,
            mountApp
        );
    });
}

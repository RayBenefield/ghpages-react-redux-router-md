import React from 'react';
import { MarkdownPreview } from 'react-marked-markdown';
import github from './github-markdown.css';

export default ({ file }) => (
    <div>
        <sub><em><b>From:</b></em> {file.filename}</sub>
        <br />
        <br />
        <MarkdownPreview
            className={github['markdown-body']}
            value={file.markdown}
            markedOptions={{ tables: true }}
        />
    </div>
);

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

const fileReducer = (state = [], action) => {
    if (action.type === 'LOAD_FILES') {
        return action.files;
    }
    return state;
};

export default combineReducers({
    routing: routerReducer,
    files: fileReducer,
    form: formReducer,
});

import React from 'react';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import 'tachyons/css/tachyons.min.css';
import configureStore from './configureStore';
import files from './pages/files';
import routes from './pages/routes';

const store = configureStore();
store.dispatch({ type: 'LOAD_FILES', files });

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(hashHistory, store);

export default () => (
    <Provider store={store}>
        <Router history={history}>
            {routes}
        </Router>
    </Provider>
);

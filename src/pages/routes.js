import _ from 'underscore';
import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { IndexRoute, Route } from 'react-router';
import MarkdownDocument from '../components/markdown-document';
import Layout from '../layout';
import Form from '../pages/form';
import DevTools from '../components/dev-tools';
import files from './files';
import './index.html';
import './404.html';
import './index.css';

const PageHandler = ({ params: { splat: page = '' } }) => (
    <MarkdownDocument file={_.findWhere(files, { slug: page })} />
);

const Main = ({ children }) => (
    <MuiThemeProvider>
        <div>
            <Layout
                style={{
                    height: '100vh',
                    display: 'flex',
                    flexDirection: 'column',
                }}
                file={children}
            />
            <DevTools />
        </div>
    </MuiThemeProvider>
);

const myForm = () => <Form onSubmit={v => console.log(v)} />;

export default (
    <Route path="/" component={Main}>
        <IndexRoute component={PageHandler} />
        <Route path="form" component={myForm} />
        <Route path="*" component={PageHandler} />
    </Route>
);

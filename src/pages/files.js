import s from 'underscore.string';

function requireAll(directory, requireContext) {
    return requireContext.keys().map((filename) => {
        const basename = s.strLeft(filename, '.md');
        const slug = basename === './readme' ? '' : basename.replace('.', directory);
        const path = s.words(slug, '/').map(s.humanize);
        return {
            slug,
            path: basename === './readme' ? [] : path,
            filename: basename === './readme' ? '' : filename.replace('./', `${directory}/`),
            markdown: requireContext(filename),
        };
    });
}

const files = requireAll('', require.context('../../', false, /.*\.md/)).concat(
    requireAll('docs', require.context('../../docs', true, /.*\.md/)),
);

export default files;

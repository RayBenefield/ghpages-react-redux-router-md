import { createStore, compose } from 'redux';
import DevTools from './components/dev-tools';
import rootReducer from './reducers';

const enhancer = compose(DevTools.instrument());

// Add the reducer to your store on the `routing` key
export default () => createStore(
    rootReducer,
    {},
    enhancer,
);

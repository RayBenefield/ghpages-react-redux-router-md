import React from 'react';
import Header from './header';
import Content from './content';

export default ({ file }) => (
    // Parent Layout Flex
    <div
        style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
        }}
    >
        <Header />
        <Content file={file} />
    </div>
);

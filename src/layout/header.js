import React from 'react';
import AppBar from 'material-ui/AppBar';
import { Link } from 'react-router';

export default () => (
    <div>
        <AppBar title={(<Link to="/">React-Redux</Link>)} style={{ height: '64px' }} />
    </div>
);

import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router';
import container from './container';

const Menu = () => (
    <Drawer
        containerStyle={{
            width: '256px',
            position: 'inherit',
        }}
        open
    >
        <MenuItem
            menuItems={[
                <Link to="docs/first">
                    <MenuItem menuItems={[]}>First</MenuItem>
                </Link>,
                <Link to="docs/testing">
                    <MenuItem menuItems={[]}>Testing</MenuItem>
                </Link>,
                <MenuItem
                    menuItems={[
                        <Link to="docs/sub/directory">
                            <MenuItem menuItems={[]}>Directory</MenuItem>
                        </Link>,
                    ]}
                >Sub</MenuItem>,
            ]}
        >Docs</MenuItem>
        <Link to="form">
            <MenuItem menuItems={[]}>Forms</MenuItem>
        </Link>
    </Drawer>
);

export default container(Menu);

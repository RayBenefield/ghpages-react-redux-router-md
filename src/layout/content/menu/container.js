import { connect } from 'react-redux';

const mapStateToProps = state => ({ files: state.files });

export default connect(
    mapStateToProps,
);

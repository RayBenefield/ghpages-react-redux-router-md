import React from 'react';
import Footer from './footer';
import Menu from './menu';

export default ({ file }) => (
    <div
        style={{
            // Child Layout Flex
            height: '100%',
            overflowX: 'auto',
            overflowY: 'hidden',

            // Parent Content Flex
            display: 'flex',
            flexDirection: 'column',
        }}
    >
        <div
            style={{
                // Child Content Flex
                flex: 1,
                overflowX: 'visible',
                overflowY: 'hidden',

                // Parent Content Flex
                display: 'flex',
                flexDirection: 'row',
            }}
        >
            <Menu />
            <div
                style={{
                    padding: '30px',
                    overflowY: 'auto',
                }}
            >
                {file}
            </div>
        </div>
        <Footer />
    </div>
);

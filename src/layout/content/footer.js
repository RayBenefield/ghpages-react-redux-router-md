import React from 'react';
import Paper from 'material-ui/Paper';

export default () => (
    // Child Content Flex
    <Paper
        style={{
            height: '64px',
            backgroundColor: '#eee',
            borderTop: '1px solid #e0e0e0',
        }}
    />
);
